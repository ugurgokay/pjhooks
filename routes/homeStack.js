import React from 'react';
import {Image,Text,Button,View,AsyncStorage, TouchableOpacity, Dimensions} from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer,createSwitchNavigator } from 'react-navigation';

import { Icon } from 'react-native-elements';


import AuthLoadingScreen from '../redux/AuthLoading';

import MainCategory from '../screens/Categories/categories';
import SubCategory from '../screens/Categories/subCategories';
import ProductDetail from '../screens/Product/productDetail';
import ProductList from '../screens/Product/productList';
import Cart from '../screens/Product/cart'

import NewsPage from '../screens/News/list'
import NewsDetail from '../screens/News/detail';

import ContactPage from '../screens/contact';

import OrderHistory from '../screens/User/orderHistory';
import UserInformation from '../screens/User/userSettings';

import UserCreateScreen from '../screens/Login/createAccount'
import LoginScreen from '../screens/Login/login'
import ForgotPasswordScreen from '../screens/Login/forgotPassword';
import FullScreenImage from '../components/FullScreenImage';

import animAppScreen from '../screens/Login/animApp';

// TODO
// logoOnClick navigate main



const screens= {
    // Login: {
    //     // screen: OrderHistory,
    //     screen: animAppScreen,
    //     // screen: Cart,
    //     // screen: NewsPage,
    //     // screen: ProductDetail,
    //     navigationOptions: ({navigation}) => ({
    //         title: 'Romaster',
    //         headerLeft: () => null,
    //         headerTitle: () =>  <TitleLogo /> ,
    //         headerRight: () => <CartButton navigation={navigation} /> 
    //     })

    // },
    MainCategory: {
        screen: MainCategory,
        navigationOptions: ({navigation}) => ({
            title: 'Romaster',
        headerLeft: () => <MainLeftNews navigation={navigation} />,
            headerTitle: () =>  <TitleLogo navigation={navigation} /> ,
            headerRight: () => <CartButton navigation={navigation} /> 
        })
    },
    SubCategory: {
        screen: SubCategory,
        navigationOptions: ({navigation}) => ({
            title: 'Romaster',
            headerBackTitle: 'Back',
            headerTitle: () =>  <TitleLogo navigation={navigation} /> ,
            headerRight: () => <CartButton navigation={navigation} /> 
        }),
    },
    List: {
        screen: ProductList,
        navigationOptions: ({navigation}) => ({
            title: 'Romaster',
            headerBackTitle: () => 'Back',
            headerTitle: () =>  <TitleLogo navigation={navigation} /> ,
            headerRight: () => <CartButton navigation={navigation} /> 
        })
    },
    Detail: {
        screen: ProductDetail,
        navigationOptions: ({navigation}) => ({
            title: 'Romaster',
            headerBackTitle: 'Back',
            headerTitle: () =>  <TitleLogo navigation={navigation} /> ,
            headerRight :() => <CartButton navigation={navigation} /> 
        })
    },
    FullScreenImage : {
        screen: FullScreenImage,
        navigationOptions: ({navigation}) => ({
            title: 'Romaster',
            headerBackTitle: 'Back',
            headerTitle: () =>  <TitleLogo navigation={navigation} /> ,
            headerRight :() => <CartButton navigation={navigation} /> 
        })
    },
    Contact: {
        screen : ContactPage,
        navigationOptions: ({navigation}) => ({
            title: 'Romaster',
            headerBackTitle: 'Back',
            headerTitle: () =>  <TitleLogo navigation={navigation} /> ,
            headerRight :() => <CartButton navigation={navigation} /> 
        })
    },
    Cart: {
        screen : Cart,
        navigationOptions: ({navigation}) => ({
            headerBackTitle: 'Back'
        })
    },
    NewsList: {
        screen: NewsPage,
        navigationOptions: ({navigation}) => ({
            title: 'Romaster',
            headerBackTitle: 'Back',
            headerTitle: () =>  <TitleLogo navigation={navigation} /> ,
            headerRight :() => <CartButton navigation={navigation} /> 
        })
    },
    NewsDetail: {
        screen: NewsDetail,
        navigationOptions: ({navigation}) => ({
            title: 'Romaster',
            headerBackTitle: 'Back',
            headerTitle: () =>  <TitleLogo navigation={navigation} /> ,
            headerRight :() => <CartButton navigation={navigation} /> 
        })
    },
    OrderHistory: {
        screen : OrderHistory,
        navigationOptions: ({navigation}) => ({
            title: 'Romaster',
            headerBackTitle: () => 'Back',
            headerTitle: () =>  <TitleLogo navigation={navigation} /> ,
            headerRight : () => <CartButton navigation={navigation} /> 
        })
    },
    
    UserInformation: {
        screen : UserInformation,
        navigationOptions: ({navigation}) => ({
            title: 'Romaster',
            headerBackTitle: 'Back',
            headerTitle: () =>  <TitleLogo navigation={navigation} /> ,
            headerRight: () => <CartButton navigation={navigation} /> 
        }) 
    }
}

const _retrieveCartCount = async () => {
    try {
    AsyncStorage.getItem("_CART_PRODUCT_").then((value) => {
        // setCartProducts(JSON.parse(value).length);
        console.log('////')
        let count = JSON.parse(value);
        console.log(count.length)
    })
    } catch (error) {
        console.log(error);
    }
  };

const MainLeftNews = ({navigation}) => {
    return(
        <TouchableOpacity onPress={() => navigation.navigate('NewsList')}>
            <Icon
                // onPress={() => navigation.navigate('Cart')} 
                name='play-circle'
                style={{marginLeft:15}}
                size={25}
                type='font-awesome'
                /> 
        </TouchableOpacity>
    )
};

const TitleLogo = ({navigation}) => {
    return (
        <TouchableOpacity style={{flex: 1}}
        onPress={() => navigation.navigate('MainCategory')}>
        <Image
            style={{top:0,left:0,width:100, flex:1,alignSelf:'center', justifyContent:'space-between',  height:50,  resizeMode:'contain'}}
            source={require('../assets/images/logo.png')}
        />
        </TouchableOpacity>
    )
};
const CartButton = ({navigation}) => {
        return (
            <View style={{marginRight:20}}> 
            {/* <View style={{borderRadius:10,zIndex:45,right:-10,width:15,alignContent:'center',backgroundColor:'orange',position:'absolute'}}> 
                <Text style={{textAlign:'center',color:'#fff'}}>4</Text> 
            </View> */}
            <Icon
                name='shopping-cart'
                type='font-awesome'
                onPress={() => navigation.navigate('Cart')} /> 
            </View>
        )
};


const AppStack = createStackNavigator(screens)

const AuthStack = createStackNavigator(
        {
            LoginRoute:{
                screen : LoginScreen,
                navigationOptions: { headerShown: false }
            },
            UserCreate: {
                screen: UserCreateScreen,
                navigationOptions: { headerShown:false }
            },
            ForgotPassword: {
                screen: ForgotPasswordScreen,
                navigationOptions: { headerShown:false }
            }
        }
)

export default createAppContainer(createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        Auth : AuthStack,
        App: AppStack

    },  
    {
        initialRouteName: 'AuthLoading'
    }
));