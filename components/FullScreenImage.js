import React, {Component} from 'react';
import {Dimensions,Image} from 'react-native';

import ImageZoom from 'react-native-image-pan-zoom';




export default function FullScreenImage({navigation}){

    let img = navigation.getParam('picture')
    
        return (
            <ImageZoom 
            style={{backgroundColor:'#000', flex:1, justifyContent:'center', alignItems:'center', alignContent:'center', alignSelf:'center', paddingTop:200}}
            cropWidth={Dimensions.get('window').width}
            cropHeight={Dimensions.get('window').height}
            imageWidth={Dimensions.get('window').width}
            imageHeight={Dimensions.get('window').height}>
            <Image style={{width:Dimensions.get('window').width, height: 400}}
            source={{uri:img}} />
            </ImageZoom>
            )
}
        