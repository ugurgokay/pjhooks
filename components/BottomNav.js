import React, { useEffect, useState } from "react";
import {
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  AsyncStorage,
} from "react-native";
import { Icon } from "react-native-elements";
import axios from "axios";

var eP = require("../constants.js");

import en from "../assets/language/en-EN";
import ru from "../assets/language/ru-RU";

import i18n from "i18n-js";

i18n.translations = {
  en,
  ru,
};

export default function BottomNav({ navigation, lang }) {
  const _retrieveData = async () => {
    try {
      await AsyncStorage.getItem("userId").then((res) => {
        const fetchData = async () => {
          const result = await axios(eP.apiUrl + eP.user + "/" + res);
          navigation.navigate("UserInformation", { userInfo: result.data[0] });
        };
        fetchData();
      });
    } catch (error) {
      console.log(error);
    }
  };

  const goToUserSettings = () => {
    _retrieveData();
  };

  return (
    <View style={styles.absoluteContainer}>
      <TouchableOpacity
        style={styles.divisionFour}
        onPress={() => navigation.navigate("MainCategory")}
      >
        <Icon name="home" size={20} type="font-awesome" />
        <Text style={styles.divisionText}> {i18n.t("btnHomePage")} </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.divisionFour}
        onPress={() => navigation.navigate("Contact")}
      >
        <Icon name="envelope" size={16} type="font-awesome" />
        <Text style={styles.divisionText}> {i18n.t("btnContact")} </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.divisionFour}
        onPress={() => navigation.navigate("OrderHistory")}
      >
        <Icon name="archive" size={16} type="font-awesome" />
        <Text style={styles.divisionText}> {i18n.t("btnMyOrders")} </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.divisionFour}
        onPress={() => goToUserSettings()}
      >
        <Icon name="user-circle" size={16} type="font-awesome" />
        <Text style={styles.divisionText}>{i18n.t("btnMySettings")}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  absoluteContainer: {
    flex: 1,
    paddingBottom: 5,
    paddingTop: 5,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "space-evenly",
    flexDirection: "row",
    flexWrap: "wrap",
    width: "100%",
    position: "absolute",
    paddingBottom: 20,
    bottom:0,
    borderTopColor: "#ccc",
    borderTopWidth: 2,
  },
  divisionFour: {
    width: Dimensions.get("window").width / 4 - 20,
    // margin: 10,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
  },
  divisionText: {
    fontWeight: "400",
    fontSize: 12,
  },
});
