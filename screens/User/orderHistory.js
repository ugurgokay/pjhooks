import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  RefreshControl,
  View,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  Image,
  SafeAreaView,
} from "react-native";
import axios from "axios";

import moment from "moment";
var eP = require("../../constants.js");
import BottomNav from "../../components/BottomNav";
import { TouchableOpacity } from "react-native-gesture-handler";

import en from "../../assets/language/en-EN";
import ru from "../../assets/language/ru-RU";

// import * as Localization from 'expo-localization';
import i18n from "i18n-js";

i18n.translations = {
  en,
  ru,
};

//   i18n.locale = 'Localization.locale;'
//   {i18n.t('welcome')} {i18n.t('name')}

// i18n.locale = 'ru';

export default function OrderHistory({ navigation }) {
  const [userOrders, setUserOrders] = useState([]);
  const [filteredOrders, setFilteredOrders] = useState([]);

  const [userId, setUserId] = useState();
  const [isLoading, setLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [pendingOrders, setPendingCount] = useState()
  const [approvedOrders, setApprovedCount] = useState()

  const [orders, setOrders] = useState(true);

  const [filteredOrderButton, setFilteredOrderButton] = useState(true);

  useEffect(() => {
    _retrieveData("userId");
  }, []);

  useEffect(() => {
    _getAllOrders();
  }, [userId]);

  const _getAllOrders = () => {
    const fetchData = async () => {
      const result = await axios(eP.apiUrl + eP.orders + "/" + userId);
      setUserOrders(result.data);


      
    };
    fetchData();
  };

  const _filterOrders = (type = "PENDING") => {
    const ab = userOrders.filter((itm) => itm.orderType == type);
    setFilteredOrders(ab);
    setFilteredOrderButton(type == "PENDING" ? true : false);

    // alert(i18n.t('txtHistory'))
  };

  useEffect(() => {
    // console.log(userOrders.filter((itm) => itm.orderType == 'PENDING').length);
    setPendingCount(userOrders.filter((itm) => itm.orderType == 'PENDING').length )
    setApprovedCount(userOrders.filter((itm) => itm.orderType != 'PENDING').length )

    console.log('///')
    // console.log(countOrders)
    console.log('///')

  }, [userOrders])

  useEffect(() => {
    userOrders.forEach((node) => {
      node.orderDE = JSON.parse(node.orderDetail);
    });
    setLoading(false);
    _filterOrders();
  }, [userOrders]);

  const _retrieveData = async (key) => {
    try {
      await AsyncStorage.getItem(key).then((res) => {
        setUserId(res);
      });
    } catch (error) {
      console.log(error);
    }
  };

  const SubProducts = ({ item }) => {
    return (
      <View>
        {item.value == "" ? (
          <View></View>
        ) : (
          <View style={styles.productTypesView}>
            <Text style={{ width: "30%", color: "#ff0000" }}>
              {item.productType}{" "}
            </Text>
            <Text style={{ color: "#0520b5" }}>
              {item.value} {i18n.t("serie")}
            </Text>
            <Text style={{ color: "#ff0000" }}>
              {item.productTotalPrice} $ =
            </Text>
            <Text style={{ color: "#ff0000" }}>
              {" "}
              {item.value *
                item.productSubPiece *
                parseInt(item.productTotalPrice)}{" "}
              $
            </Text>
          </View>
        )}
      </View>
    );
  };

  const DetailItem = ({ item }) => {
    return (
      <View style={styles.container}>
        <View style={styles.orderItem}>
          <Image style={styles.orderItemImage} source={{ uri: item.picture }} />
          <View style={{ width: "70%", marginLeft: 10 }}>
            <Text>{item.productName} </Text>
            <FlatList
              data={item.productTypes}
              keyExtractor={(item, index3) => String(index3)}
              renderItem={({ item }) => <SubProducts item={item} />}
            />
          </View>
        </View>
      </View>
    );
  };

  const OrderItem = ({ item }) => (
    <View style={styles.orderNode}>
      {/* <TouchableOpacity > */}
      <Text style={styles.orderType}>
        {moment(item.createTime).format("MMMM D YYYY, h:mm a")} -
        {item.orderType === "PENDING"
          ? " " + i18n.t("txtAwaitingApproval")
          : " " + i18n.t("txtApproved")}{" "}
      </Text>
      <FlatList
        data={item.orderDE}
        style={{ height: "100%" }}
        keyExtractor={(item, index) => String(index)}
        renderItem={({ item }) => <DetailItem item={item} />}
      />
      {/* </TouchableOpacity> */}
    </View>
  );

  // const NoOrderView = ({}) => (
  //     <Text>No Order</Text>
  // )

  function refreshOrders({}) {
    alert("refed");
    setRefreshing(false);
  }

  return (
    <View style={styles.container}>
      {isLoading ? (
        <ActivityIndicator
          size="large"
          color="#0000ff"
          style={styles.loading}
        />
      ) : (
 
        <View>
        <View style={styles.tabButtons}>
        <TouchableOpacity
        style={[
              filteredOrderButton
                ? styles.filteredApprovalTouchable
                : styles.approbalTouchable,
            ]}
         onPress={() => _filterOrders()}>
          <Text
            style={[
              filteredOrderButton
                ? styles.filteredApprovalButton
                : styles.approvalButtons,
            ]}
          >
            {i18n.t("txtPending")} {i18n.t("txtOrders")} ({pendingOrders})
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
                   style={[
              !filteredOrderButton
                ? styles.filteredApprovalTouchable
                : styles.approbalTouchable,
            ]}
         onPress={() => _filterOrders("APPROVED")}>
          <Text
            style={[
              !filteredOrderButton
                ? styles.filteredApprovalButton
                : styles.approvalButtons,
            ]}
          >
            {i18n.t("txtApproved")} {i18n.t("txtOrders")} ({approvedOrders})
          </Text>
        </TouchableOpacity>
      </View>
          <Text style={styles.headerText}>
            {i18n.t("txtOrder")} {i18n.t("txtHistory")}{" "}
          </Text>
          <View style={styles.mpBsi}>
            <SafeAreaView>
              <FlatList
                data={filteredOrders}
                style={{ height: "100%", flexDirection: "column" , marginTop:40}}
                keyExtractor={(item, index) => String(index)}
                renderItem={({ item }) => <OrderItem item={item} />}
              />
            </SafeAreaView>
          </View>
        </View>
      )}
    
      <BottomNav navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  mpBsi: {
    marginBottom: 285,
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  orderNode: {
    backgroundColor: "#fff",
    padding: 10,
    margin: 10,
    marginTop: 0,
    borderBottomColor: "#e2e2e2",
    borderBottomWidth: 2,
    borderRightColor: "#e2e2e2",
    borderRightWidth: 2,
    borderRadius: 4,
    flex: 1,
    flexDirection: "column",
  },
  orderItem: {
    margin: 10,
    flexDirection: "row",
    flexWrap: "wrap",
    flex: 1,
  },
  orderItemImage: {
    width: "25%",
    height: 70,
    borderRadius: 5,
  },
  productTypesView: {
    flexDirection: "row",
    flexWrap: "wrap",
    backgroundColor: "#eee",
    margin: 2,
    padding: 2,
    borderRadius: 3,
    borderBottomColor: "#e6dbdb",
    borderBottomWidth: 1,
    borderRightColor: "#e6dbdb",
    borderRightWidth: 1,
    flex: 1,
    justifyContent: "space-between",
  },
  headerText: {
    fontWeight: "300",
    fontSize: 24,
    marginTop: 10,
    // right: 20,
    width: "100%",
    textAlign: "center",
    marginBottom: 10,
  },
  item: {
    marginTop: 125,
    padding: 10,
    textAlign: "right",
    backgroundColor: "rgba(255,255,255,0.8)",
    fontSize: 24,
  },
  orderType: {
    backgroundColor: "#e2e2e2",
    width: "100%",
    textAlign: "right",
    padding: 5,
  },
  loading: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  tabButtons: {
    position: "absolute",
    top: 40,
    paddingTop: 10,
    width: "100%",
    backgroundColor: "#fff",
    // flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    // flexWrap: "nowrap",
    // borderBottomColor: "#ccc",
    // borderBottomWidth: 2,
    zIndex:99
  },
  approvalButtons: {
    fontWeight: "300",
    fontSize:16,
    width:'100%',
    flex:1,
    paddingTop: 5,
    backgroundColor:'#fff',
    paddingBottom: 10,
      borderBottomWidth:2,
    borderBottomColor:'#000',
  },
  filteredApprovalButton: {
    fontWeight: "700",
    paddingTop: 5,
    width:'100%',
    fontSize:16,
    flex:1,
    color:'#000',
    paddingBottom: 10,
    borderBottomWidth:2,
    borderBottomColor:'#000',
  },
  filteredApprovalTouchable: {
    width:'100%',
    flex:1,
    borderBottomColor:'#444',
    borderBottomWidth:2
  },
  approbalTouchable: {
    width:"100%",
    borderBottomWidth:2,
    borderBottomColor:'#ccc',
    flex:1
  }
});
