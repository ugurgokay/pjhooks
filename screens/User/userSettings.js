import React, {useState,useEffect} from 'react';
import {StyleSheet, Alert, Text, View, AsyncStorage, ScrollView, KeyboardAvoidingView, TouchableWithoutFeedback,Keyboard,Button, ImageBackground, ToolbarAndroid} from 'react-native';
import {Formik} from 'formik';
import axios from 'axios';
import {TextField} from 'react-native-material-textfield';

var eP = require('../../constants.js');
import BottomNav from '../../components/BottomNav';


import en from "../../assets/language/en-EN";
import ru from "../../assets/language/ru-RU";

// import * as Localization from 'expo-localization';
import i18n from 'i18n-js';

i18n.translations = {
    en,
    ru,
  };


export default function UserSettings({ navigation }){




  const removeUserToken = () => {



    Alert.alert(
      i18n.t('btnLogout'),
      i18n.t('txtSureToLogout'),
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => 
        {
          try {
            AsyncStorage.getAllKeys()
                .then(keys => AsyncStorage.multiRemove(keys))
                // .then(() => alert('success'));  
      } catch (error) {
        console.log(error);
      }
      
          try {
          AsyncStorage.removeItem('userToken')
            .then((data) => {
                navigation.navigate('Auth')
                dispatch(loading(false));
                dispatch(removeToken(data));
            })
            .catch((err) => {
                dispatch(loading(false));
                dispatch(error(err.message || 'ERROR'));
            })
      
          } catch (error) {
            console.log(error);
          }
        }
 }
      ],
      { cancelable: false }
    );
  




    }
// TODO
// 7. Hesap BilgilerimKullanıcı Telefon numarası dışında her türlü bilgisini güncelleyebilecektir. Fakat Güncelleme olduğunda Admin'e bu bilgiler güncellendi diye haber gidecektir.



    let userObj = navigation.getParam('userInfo')

    console.log(userObj)
  



    let initialObj = {eMail: userObj.eMail, name: userObj.name, surname: userObj.surname,  phoneNumber: '+'+userObj.countryCode +'-'+ userObj.phoneNumber}
    return (
        <KeyboardAvoidingView
        behavior={Platform.OS == "ios" ? "padding" : "height"}
        style={styles.container}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}  >
              <View >
                <Text style={styles.headerTitle}>{i18n.t('btnMySettings')} </Text>
                <Formik
                  initialValues={initialObj}
                  enableReinitialize={true}
                  
                  // initialValues={initialObj}
                  onSubmit={(values, actions) => {

                      console.log(JSON.stringify(values,null,2));

                      alert("OK")
                    //   setTimeout(() => {
                    //    console.log(JSON.stringify(values, null, 2));
                    //    actions.setSubmitting(true);
    
    
                    //   //  this.props.navigation.navigate('Login');
    
                    //     // values.mainCategoryId = !values.mainCategoryId ? values.mainCategoryId = 0  : values.mainCategoryId
                    //     // navigation.navigate('Login');
                    //     // navigation.navigate('Detail');
                    //           axios.post(eP.apiUrl +  eP.user, {
                    //             name: values.name,
                    //             surname: values.surname,
                    //             countryCode : values.countryCode,
                    //             eMail : values.eMail,
                    //             password: "UNSET_11",
                    //             phoneNumber: values.phoneExt+'-'+ values.phoneNumber,
                    //             status: false
                    //           })
                    //           .then(function (response) {
                    //             // console.log(response.data); //-> UUID
                    //             alert('Üyelik talebiniz alınmıştır. Onaylanması durumunda e-posta ile bilgilendirileceksiniz.' )
                    //             // navigation.pop();
                    //           //   debugger;
                    //           })
                    //           .catch(function (error) {
                    //             console.log(error);
                    //             alert('Bir Hata Oluştu Lütfen tekrar deneyiniz')
                    //           //   debugger;
                    //           });
                    //             actions.setSubmitting(false);
                    //           }, 1000);
                            }}
                          // onSubmit={values =>  {
                          //   alert(values.toString());
                          //   console.log(values)
                            
                          //   }
                          // }
                        >
                    {({ handleChange, handleBlur, handleSubmit, values }) => (
                      <ScrollView style={styles.scrollView}>
                        <View style={styles.formView}>
                          <TextField
                            label={i18n.t('txtHistory')} 
                            value={values.name}
                            onChangeText={handleChange('name')}
                            onBlur={handleBlur('name')}
                          />
                        </View>
                      
    
                        <View style={styles.formView}>
                          <TextField
                            label={i18n.t('txtSurname')} 
                            onChangeText={handleChange('surname')}
                            onBlur={handleBlur('surname')}
                            value={values.surname}
                          />
                        </View>
    
    
                        <View style={styles.formView}>
                          <TextField
                            label={i18n.t('txtPhone')} 
                            onChangeText={handleChange('phoneNumber')}
                            keyboardType="numeric"
                            disabled={true}
                            style={styles.alignSeven}
                            onBlur={handleBlur('phoneNumber')}
                            value={values.phoneNumber}
                          />
                        </View>
{/*     
                        <View style={styles.formView}>
                        <TextField
                            label="Country"
                            onChangeText={handleChange('countryCode')}
                            onBlur={handleBlur('countryCode')}
                            value={values.countryCode}
                          />
                        </View> */}
                        <View style={styles.formView}>
                        <TextField
                          keyboardType="email-address"
                          label={i18n.t('txtEMail')} 
                          onChangeText={handleChange('eMail')}
                          onBlur={handleBlur('eMail')}
                          value={values.eMail}
                        />
                        </View> 
    
                        <Button onPress={handleSubmit} title={i18n.t('btnSubmit')}  />
                        <Text style={styles.textHint}>
                         {i18n.t('msgSubmissionInfo')} 
                        </Text>
                        <Button style={styles.divisionFour} onPress={() => {
                // console.log('sajkdh');
                // TODO eminmisniz çıkmak istediğinize sor
                removeUserToken();
                // navigation.navigate('Auth')
                } }  title={i18n.t('btnLogout')}  /> 
                      </ScrollView>
                    )}
                  </Formik>
                  <BottomNav style={{position:'absolute', bottom:0,zIndex:99 }} navigation={navigation} />
                </View>
              </TouchableWithoutFeedback>
              </KeyboardAvoidingView>
    
      )

    
}

const styles = StyleSheet.create({
  divisionFour: {
    bottom:100
  },
    container: {
        flex:1,
        backgroundColor: '#fff',
        marginBottom:42

    },
    item: {
        marginTop: 125,
        padding:10,
        textAlign:'right',
        backgroundColor: 'rgba(255,255,255,0.8)',
        fontSize:24
    },
    scrollView: {
      marginBottom:60
    },
      formView: {
        margin:10,
        marginTop:0
      },
      headerTitle : {
        fontSize:18,
        textAlign:'center',
        marginTop:20
      },
      textHint: {
        marginTop:20,
        fontWeight:'bold',
        margin:40,
        textAlign:'center'
      },

})