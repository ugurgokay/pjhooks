import React, { useState, useEffect } from "react";
import axios from "axios";

import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { Icon } from "react-native-elements";

import BottomNav from "../../components/BottomNav";

var eP = require("../../constants.js");

export default function NewsPage({ navigation }) {
  const [news, setNews] = useState();

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(eP.apiUrl + eP.news);
      setNews(result.data);
    };
    fetchData();
  }, []);

  return (
    <View style={styles.container}>
      <FlatList
        style={styles.container}
        keyExtractor={(item, index) => index.toString()}
        data={news}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            onPress={() => navigation.push("NewsDetail", { item: item })}
          >
            <View style={styles.newsItem}>
              <View style={styles.playButton}>
                {item.video != "" ? (
                  <Icon
                    name="play-circle-o"
                    size={20}
                    color="#f50"
                    type="font-awesome"
                  />
                ) : (
                  <></>
                )}
              </View>
              <Image
                style={styles.itemImage}
                source={{ uri: item.coverImage }}
              />
              <View style={styles.itemContent}>
                <Text style={styles.itemTitle}>{item.header}</Text>
                <Text style={styles.itemDescription}>{item.description} </Text>
              </View>
              <View style={styles.moreButton}>
                <Icon name="chevron-right" type="font-awesome" />
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
      <BottomNav navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  playButton: {
    marginLeft: 10,
    position: "absolute",
    zIndex: 9909,
    height: 30,
    width: 30,
    top: 10,
    left: 60,
  },
  newsItem: {
    padding: 10,
    margin: 5,
    position: "relative",
    marginHorizontal: Platform.OS === "ios" ? -15 : 0,
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    borderBottomWidth: 1,
    borderBottomColor: "#ddd",
    marginLeft: 10,
    marginRight: 10,
  },
  itemImage: {
    width: "25%",
    height: 70,
    alignItems: "flex-start",
    borderRadius: 5,
    resizeMode: "cover",
  },
  itemContent: {
    width: "70%",
    paddingLeft: 15,
  },
  itemTitle: {
    fontWeight: "400",
  },
  itemDescription: {
    fontWeight: "200",
  },
  moreButton: {
    width: "5%",
    paddingTop: 30,
  },
});
