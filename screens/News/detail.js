import React, { useState, useEffect } from "react";

import { View, Text, Image, StyleSheet } from "react-native";
import { WebView } from "react-native-webview";

import BottomNav from "../../components/BottomNav";
import { ScrollView } from "react-native-gesture-handler";

var eP = require("../../constants.js");

export default function NewsDetail({ navigation }) {
  let newsDetail = navigation.getParam("item");

  return (
    <View style={styles.container}>
      <ScrollView style={styles.scrollContainer}>
        {newsDetail.video == "" ? (
          <Image
            style={styles.itemImage}
            source={{ uri: newsDetail.coverImage }}
          />
        ) : (
          <WebView
            style={styles.video}
            javaScriptEnabled={true}
            source={{ uri: newsDetail.video }}
          />
        )}
        <Text style={styles.header}>{newsDetail.header}</Text>

        <Text style={styles.content}>{newsDetail.context}</Text>
      </ScrollView>
      <BottomNav navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  video: {
    height: 200,
    width: "100%",
  },
  scrollContainer: {
    marginBottom: 30,
  },
  newsItem: {
    padding: 20,
    margin: 5,
    position: "relative",
    marginHorizontal: Platform.OS === "ios" ? -15 : 0,
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    borderBottomWidth: 1,
    borderBottomColor: "#ddd",
    marginLeft: 10,
    marginRight: 10,
  },
  itemImage: {
    width: "100%",
    height: 200,
    // alignItems: "flex-end",
    resizeMode: "contain",
  },
  itemContent: {
    width: "70%",
    paddingLeft: 15,
  },
  header: {
    fontWeight: "400",
    margin: 10,
    fontSize: 24,
    textAlign: "center",
  },
  content: {
    fontWeight: "200",
    margin: 10,
  },
  moreButton: {
    width: "5%",
    paddingTop: 30,
  },
});
