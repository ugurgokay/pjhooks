import React, { useState } from 'react';
import { Text, StyleSheet,Dimensions, Image, Animated, Button, Option,View, ScrollView, TextInput, PixelRatio, Switch,TouchableWithoutFeedback,KeyboardAvoidingView,Keyboard  } from 'react-native';
import { Formik } from 'formik';
import axios from 'axios';
import { TextField } from 'react-native-material-textfield';
import PhoneInput from 'react-native-phone-input'
import CountryPicker from 'react-native-country-picker-modal'
import DatePicker from 'react-native-datepicker'


export default function animApp({ navigation}){

  const [date, setDate] = useState({date:'2016-05-01'});

    const [countryCode, setCountryCode] = useState('FR')
    const [country, setCountry] = useState(null)
    const onSelect = (country) => {
      setCountryCode(country.cca2)
      setCountry(country)

      console.log(country);
    }

    const [withCountryNameButton, setWithCountryNameButton] = useState(
      true,
    )
    const [withFlag, setWithFlag] = useState(true)
    const [withFilter, setWithFilter] = useState(true)
    const [withAlphaFilter, setWithAlphaFilter] = useState(true)
    const [withCallingCode, setWithCallingCode] = useState(true)
    const [tabs, setTabs] = useState(true)

    let moveAnimation = new Animated.ValueXY({ x: 10, y: 450 })

    const  _moveBall = () => {
      Animated.spring(moveAnimation, {
        toValue: {x: 100, y: -10},
      }).start()
    }

    return (

        <View style={styles.container}>



<Animated.View style={[styles.tennisBall, moveAnimation.getLayout()]}>
          <TouchableWithoutFeedback style={styles.button} onPress={() =>_moveBall()}>
            <Text style={styles.buttonText}>Press</Text>
          </TouchableWithoutFeedback>        
        </Animated.View>
{/* <Text style={styles.welcome}>Welcome to Country Picker !</Text>
<DatePicker
        style={{width: 200}}
        date={date}
        // mode="date"
        placeholder="select date"
        format="YYYY-MM-DD"
        minDate="2016-05-01"
        maxDate="2016-06-01"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel" */}
        {/* // onDateChange={(date) => {setDate({date: date})}} */}
        {/* /> */}
      {/* <Option
        title='With country name on button'
        value={withCountryNameButton}
        onValueChange={setWithCountryNameButton}
      />  */}
      {/* <Option title='With flag' value={withFlag} onValueChange={setWithFlag} />
      <Option
        title='With filter'
        value={withFilter}
        onValueChange={setWithFilter}
      />
      <Option
        title='With calling code'
        value={withCallingCode}
        onValueChange={setWithCallingCode}
      />
       <Option
        title='With alpha filter code'
        value={withAlphaFilter}
        onValueChange={setWithAlphaFilter}
      /> */}
      {/* <CountryPicker
        {...{
          countryCode,
          withFilter,
          withFlag,
          withCountryNameButton,
          withAlphaFilter,
          withCallingCode,
          onSelect,
        }}
        // visible
      />
      <Text style={styles.instructions}>Press on the flag to open modal</Text>
      {country !== null && (
        <Text style={styles.data}>{JSON.stringify(country, null, 2)}</Text>
      )} */}
    
          {/* <View>
             {tabs && <Text>Tab1</Text> }
          </View>
          <View>
            {!tabs && <Text>Tab2</Text> }
          </View>

          <Button onPress={() => setTabs(true)} title="tab1"/>
          <Button onPress={() => setTabs(false)} title="tab2"/> */}



        </View>
)


}



const styles = StyleSheet.create({
    absoluteBottom: {
      position: 'absolute'
    },
    container: {
      backgroundColor: '#fff',
      alignItems: 'center',
      marginTop:0,
      flex:1,
      paddingTop:50
    },
    tennisBall: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'greenyellow',
      borderRadius: 100,
      width: 100,
      height: 100,
      position:'absolute',
      zIndex: 999
    },
    button: {
      paddingTop: 24,
      paddingBottom: 24,
    },
    buttonText: {
      fontSize: 24,
      color: '#333',
    }
});