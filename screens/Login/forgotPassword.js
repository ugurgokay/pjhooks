import React, { useState } from "react";
import {
  Text,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  ScrollView,
  Button,
  Keyboard,
  View,
  Image,
} from "react-native";

import { TextField } from "react-native-material-textfield";

var eP = require("../../constants.js");
const _LANG = require('../../assets/language/en-EN.js');

export default function ForgotPassword({ navigation }) {
  const [email, setEmail] = useState();


  
    _onValidated = () => {
        alert(_LANG.alrtForgotPasswordSuccess)
        navigation.pop()

        //TODO BE chores
        // -- 
        // ForgotPassword
        //   Design
        //     Sonlandır
        //     Loginde yazdığı telefonu direk taşı forgot pw tarafına 
        //     telefon numarası girdikten sonra 
        //         ALERT (onay geldikten sonra şifreniz sıfırlanacaktır diye)
        //       [Şifremi unttuğunda telefon numarası yazılacak ve telefn numarasına onay mekanizması kurgulanacaktır.}
        //       Backendde kaç kere şifre istediğinin counterini bir kere daha arttırt
      
        //       3. Şifremi UnuttumKullanıcı telefon numarasını girdikten sonra Göndere basacak ve talebiniz alınmıştır onaylandığında bilgilendirileceksiniz diye bir mesaj çıkarılacak. Bu talep Admin e gidecek admin onayladığında yeni şifre kullanıcıya Mail aracılığı ile iletilecektir. Adminde bulunan Kullanıcının kaç kere şifre istediği bölümü güncellenecektir
    }

  _onPressLogin = () => {
    if (!email) {
      alert(_LANG.alrtEmptyField);
      return;
    } else {
      _onValidated();
    }
  };


  return (
    <KeyboardAvoidingView style={styles.container}>
      <TouchableWithoutFeedback
        style={styles.container}
        onPress={Keyboard.dismiss}
      >
        <ScrollView style={styles.sView}>
          <View style={styles.container}>
            <Image
              style={styles.loginLogo}
              source={require("../../assets/images/logo.png")}
            />
            <View style={styles.form}>
              <Text>{_LANG.txtForgotPassword}</Text>
              <View>
                <TextField
                  label={_LANG.lblPhoneNum}
                  onChangeText={(un) => setEmail(un)}
                />
              </View>
              <Button
                onPress={() => _onPressLogin()}
                title={_LANG.btnRequest}
                color="#3897f1"
              />
            </View>
          </View>
        </ScrollView>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    alignItems: "center",
    height: "100%",
    width: "100%",
    flex: 1,
    justifyContent: "center",
  },
  sView: {
    height: "100%",
    width: "100%",
  },
  form: {
    flex: 1,
    justifyContent: "center",
    width: "80%",
  },
  loginLogo: {
    width: 300,
    height:300,
    resizeMode: 'contain',
    marginTop: 30
  }
});
