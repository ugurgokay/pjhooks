import React, { useState,useEffect } from 'react';
import { Text, ActivityIndicator, StyleSheet,Dimensions, Image, Button, View, ScrollView, TextInput, PixelRatio, Switch,TouchableWithoutFeedback,KeyboardAvoidingView,Keyboard, ToolbarAndroid, SafeAreaView, Platform  } from 'react-native';
import { Formik, Form } from 'formik';
import axios from 'axios';
import { TextField } from 'react-native-material-textfield';
import CountryPicker from 'react-native-country-picker-modal'
import DateTimePicker from '@react-native-community/datetimepicker';

import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location'
import i18n from 'i18n-js';



var eP = require('../../constants.js');

// TODO
// gender seçimi
// ülke seçimi
//   onBtn Click start activity Indicator then falsify it 
// analytics ve crashlytics

export default function CreateAccount({ navigation}){

  
  const [hasLocationPermissions, setHasLocationPermissions] = useState(false);
  const [locationResult, setLocationResult] = useState(null)
  const [mapRegion, setMapRegion] = useState()
  



  let langSel = navigation.getParam('langSel')


  const [phoneExt, setPhoneExt] = useState();
  const [dateOfBirth, setDateOfBirth] = useState(new Date());


    const [countryCode, setCountryCode] = useState(i18n.locale == 'en' ? 'US' : 'RU')
    const [userCountry, setUserCountry] = useState('RU')  
    
    
    const [phoneExtCode, setPhoneExtCode] = useState();
    const [country, setCountry] = useState(null)
    const [countryPerson, setCountryPerson] = useState(null)
    const onSelect = (country) => {
      setCountryCode(country.cca2)
      setCountry(country)
      setPhoneExtCode(country.callingCode)
      console.log(country);
    }

    const _getLocationAsync = async () => {
      let { status } = await Permissions.askAsync(Permissions.LOCATION);
      if (status !== 'granted') {
       setLocationResult('Permission to access location was denied')
      } else {
       setHasLocationPermissions(true);
      }
   
      let location = await Location.getCurrentPositionAsync({});
      setLocationResult(JSON.stringify(location));

      console.log(JSON.stringify(location))
      
      // Center the map on the location we just fetched.
      // setMapRegion({ latitude: location.coords.latitude, longitude: location.coords.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 })
     };
   
   
     useEffect(() => {
       _getLocationAsync();

     }, [])
   



    const onSelectCountryCode = (country) => {
      secCountryPerson(country);
      console.log(country)
    }

    const [withCountryNameButton, setWithCountryNameButton] = useState(
      true,
    )
    const [withFlag, setWithFlag] = useState(true)
    const [withFilter, setWithFilter] = useState(true)
    const [withAlphaFilter, setWithAlphaFilter] = useState(true)
    const [withCallingCode, setWithCallingCode] = useState(true)

    const [isLoading, setIsLoading] = useState(false);


    const [date, setDate] = useState(new Date(1598051730000));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [color, setColor] = useState();
    const [display, setDisplay] = useState('default');

    const onBirthDateChange = (event, selectedDate) => {
      const currentDate = selectedDate || date;
      setShow(Platform.OS === 'ios');
      setDate(currentDate);
    };
    const showDatepicker = () => {
      showMode('date');
      setDisplay('default');
    };
    const showMode = currentMode => {
      setShow(true);
      setMode(currentMode);
    };

 
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View>
        {isLoading ? (
          <ActivityIndicator
            size="large"
            color="#0000ff"
          />
        ) : (
          <>
        <Image style={styles.loginLogo} source={require('../../assets/images/logo.png')} />

          <Text style={styles.headerTitle}>
          {langSel == 'en' ? 'Create a New Account' : 'Создать новый аккаунт' }</Text>
          <Formik
            initialValues={{ eMail: "" }}
            onSubmit={(values, actions) => {


              let loc= JSON.parse(locationResult)

               

              if (
                !phoneExtCode ||
                !values.name ||
                !values.surname ||
                !values.phoneNumber ||
                !values.eMail
              ) {
                alert("Please Fill in missing fields");
                return;
              }


              setIsLoading(true);


              setTimeout(() => {
                actions.setSubmitting(true);

                //PHONECODE
                values.phoneExt = phoneExtCode[0];
                // alert(date)

                axios.post(eP.apiUrl +  eP.user, {
                  name: values.name,
                  surname: values.surname,
                  birthday: date,
                  countryCode : values.phoneExt,//values.countryCode,
                  eMail : values.eMail,
                  password: "UNSET_11",
                  phoneNumber: (values.phoneNumber).trim(),
                  location: JSON.stringify(loc.coords.latitude+','+ loc.coords.longitude),
                  deviceType : Platform.OS.toString(),
                  status: false
                })
                .then(function (response) {
                  console.log(response.data); //-> UUID
                  alert('Your request has been taken. We will contact you soon')
                  navigation.pop();
                //   debugger;
                })
                .catch(function (error) {
                  console.log(error);
                  alert('An error occured. Please try again')
                //   debugger;
                });
                actions.setSubmitting(false);
              }, 1000);
            }}
          >
            {({ handleChange, handleBlur, handleSubmit, values }) => (
              <ScrollView style={styles.scrollView}>
                <View style={styles.formView}>
                  <TextField
                    label={langSel == 'en' ? 'Name' : 'название' }
                    value={values.name}
                    onChangeText={handleChange("name")}
                    onBlur={handleBlur("surname")}
                  />
                </View>

                <View style={styles.formView}>
                  <TextField
                    label={langSel == 'en' ? 'Surname' : 'Фамилия' }
                    onChangeText={handleChange("surname")}
                    onBlur={handleBlur("surname")}
                    value={values.surname}
                  />
                </View>
                <View style={styles.formViewDouble}>
                  <View style={styles.widthThirty}>
                    <CountryPicker
                      {...{
                        countryCode,
                        withFilter,
                        withFlag,
                        withAlphaFilter,
                        withCallingCode,
                        onSelect,
                      }}
                    />
                  </View>
                  <View style={styles.widthSeventy}>
                    <TextField
                      label={langSel == 'en' ? 'Phone Number' : 'Телефонный номер' }
                      onChangeText={handleChange("phoneNumber")}
                      keyboardType="numeric"
                      onBlur={handleBlur("phoneNumber")}
                      value={values.phoneNumber}
                    />
                  </View>
                </View>
                <View style={styles.formView}>
                  <TextField
                    keyboardType="email-address"
                    label={langSel == 'en' ? 'E-mail' : 'Эл. почта' }
                    onChangeText={handleChange("eMail")}
                    onBlur={handleBlur("eMail")}
                    value={values.eMail}
                  />
                </View>
                {/* <View style={styles.formView}>
                  <Button
                    testID="timePickerButton"
                    onPress={showDatepicker}
                    title="Birth date"
                  />
                </View>
                <View style={styles.formView}>
                  {show && (
                    <DateTimePicker
                      testID="dateTimePicker"
                      timeZoneOffsetInMinutes={0}
                      value={date}
                      mode={mode}
                      is24Hour
                      display={display}
                      onChange={onBirthDateChange}
                      style={styles.iOsPicker}
                      textColor={color || undefined}
                    />
                  )}
                </View> */}
                
                <View style={styles.formView}>
                  <Button
                    onPress={handleSubmit}
                    title={langSel == 'en' ? 'Register' : 'регистр' }
                    style={styles.registerButton}
                  />
                  <Text style={styles.textHint}>
                  {langSel == 'en' ? 'Once your submission is approved, you will be notified via phone or e-mail.' :
                  'Как только ваша заявка будет одобрена, вы получите уведомление по телефону или электронной почте.' }
                  </Text>
                </View>

              </ScrollView>
            )}
          </Formik>
          </>
        )}
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    marginTop:0,
    flex:1,
    paddingTop:50,
  },
  loginLogo : {
    height:60,
    alignSelf:'center',
    width:100
  },
  headerTitle : {
    fontSize:18,
    textAlign:'center',
    marginTop:20
  },
  textHint: {
    marginTop:20,
    fontWeight:'bold',
    textAlign:'center',
  },
  formView:{
    padding:20,
    paddingTop:0,
    paddingBottom:0
  },
  formViewDouble: {
    flex:1,
    justifyContent: 'space-around',
    flexWrap: 'wrap',
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding:20,
    paddingTop:0,
    paddingBottom:0
  },
  widthThirty: {
    width: '15%',
    zIndex:99,
    paddingTop:20
  },
  widthSeventy : {
    width: '85%',
  },
  registerButton: {
    marginTop:50,
    paddingTop:100
  },


});

