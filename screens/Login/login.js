import React, {useState,useEffect} from 'react';
import { 
  Text, 
  StyleSheet, 
  Image, 
  Button, 
  View, 
  AsyncStorage,
  KeyboardAvoidingView, 
  TouchableWithoutFeedback, 
  Keyboard,
  TouchableOpacity,
  ScrollView
 } from 'react-native';

 import { connect } from 'react-redux';
//  import { saveUserToken } from '../../redux/actions';

 

import axios from 'axios';
import {TextField} from 'react-native-material-textfield';

import CountryPicker from 'react-native-country-picker-modal'

var eP = require('../../constants.js');

import i18n from 'i18n-js';

i18n.locale = 'en';

/* TODO : 
    Login Form LoginHandler from AppJS
    ForgotPasswordForm Handlers
TODO LOGIN sonrası bitti işlemlerin hepsi birdaha olmayacak şifreyi invalide ediyoruz yani kesinliklen
  Çıkış yapta olmasın 
    Methods
      PUT - forgetPassword 
      GET - authenticate

      userToken ve ıd aynı gidiyor BUNU KESİNLİKLE AYRIŞTIR JWTYE ÇEVİR burayıda kesinliklenn
*/

// BE AUTH dönüşü bearer token gelecektir
// Login
// TODO FORGOT PASSWORD FINALIZE
// Design
//   Sonlandır
//   Arka plana bir tane gradident koy
//   onBtn Click activity indicator başlat 
//   if Logged in set UserID(hashed) to storage 
//   tüm sayfayı gez eksikleri tek tek çıkart
//   Üye Dili alt tarafta bulunan kutucuk aracılığı ile seçilecek ve sonrasında bununla devam edecek tüm işlemlerine 
//   Login olduğunda şifre invalide olacak ve tekrar istendiğinde onaya gidecek 
//   Login : Tel Ülke - Telefon 		LOGIN OLURKEN ülke seçimi + telefon numarası + şifre 
//         Şifre sorulacak sonrasında login işlemine gidecek  
//         [Login example fakebackendde admin reduxu hallet sonrada mobile geç]
//   onLogin update lastOnline login 
// Login
// Üye dilini burada seçecek sonra tekrar değiştirmek isterse ne yapalım ? log out olursa tekrar sana gelmek zorunda
// Mobile Login
// loginde şifrenin invalide olması
// Login example fakebackendde admin reduxu hallet sonrada mobile geç
// LOGIN olursa invalide et şifreyi ve last online tarihi at 
//   birde log tablosuna at adamına ne zaman giridğinide yaz oraya yani bari
//   ------
//   Üye GirişiTelefon numarası ve şifre ile giriş yapılacakÜye Girişi formunun altında kullanıcının kullanmak istediği dil bulunacak. Defaultta ingilizce seçili gelecek eğer kullanıcı RU seçerse tüm süreç rusçaya dönecektir.Eğer kullanıcı bir kere login olursa tekrar aynı kullanıcı ile başka bir şekilde login olunamayacaktır.Kullanıcı login olduğunda Adminde bulunan üye bilgileri kısmında kullanıcının Son Giriş tarihi güncellenecektir. [ve ayrıca uygulamayı her açtığında da bu tarihi güncelliyor olacağız]

export default function LoginScreen({ navigation}){

  let isLoggedIn = false;

  const [languageSelectedVal, setLanguageSelectedVal] = useState(false);
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  const [countryCode, setCountryCode] = useState('US')
  
  
  
  const [phoneExtCode, setPhoneExtCode] = useState();
  const [country, setCountry] = useState(null)
  const [countryPerson, setCountryPerson] = useState(null)
  const onSelect = (country) => {
    setCountryCode(country.cca2)
    setCountry(country)
    setPhoneExtCode(country.callingCode)
    // console.log(country);
  }
  const [withCountryNameButton, setWithCountryNameButton] = useState(
    true,
  )
  const [withFlag, setWithFlag] = useState(true)
  const [withFilter, setWithFilter] = useState(true)
  const [withAlphaFilter, setWithAlphaFilter] = useState(true)
  const [withCallingCode, setWithCallingCode] = useState(true)


  const [demoAccountShow, setShowDemoAccount] = useState(true)


  const [langSel, setLangSel] = useState('en');

  useEffect(() => {
    const fetchData = async () => {
      await axios(
        eP.apiUrl + 'showDemoAccount'
      ).then(function (response) {

        console.log(response.data)
        setShowDemoAccount(response.data)
        // TODO bearer token buraya gelecektir
        // console.log('sakjhdaskhd')
        // saveUserToken(response.data[0].id);
      })
      .catch(function (error) {
        if(!error.response)
          alert('System Error: Please Contact Administrator')
        else if (error.response.status == '401')
         alert('Your membership has not been activated yet.')
        else
        alert('Your Phone or Password is wrong')

      });
    };
    fetchData();

  }, [])

  

  const _loginChores = () => {
    console.log('USER PW Invalidate edilecek');
    console.log('Redux UserId Store edilecek');
    console.log('UserAttemptSayısı Artırılacak _UNSET_33_ gibi         // save userId + userToken to Redux');
    // navigation.navigate('App')
    
  }
  const _onValidated = () => {
    // console.log(eP.apiUrl + eP.auth + '/' +phoneExtCode[0] + username +'/'+ password);
    const fetchData = async () => {
      await axios(
        eP.apiUrl + eP.auth + '/' + phoneExtCode[0]+'-'+username +'/'+ password
      ).then(function (response) {
        // TODO bearer token buraya gelecektir
        // console.log('sakjhdaskhd')
        saveUserToken(response.data[0].id);
      })
      .catch(function (error) {

        
        if(!error.response)
          alert('System Error: Please Contact Administrator')
        else if (error.response.status == '401')
         alert('Your membership has not been activated yet.')
        else
        alert('Your Phone or Password is wrong')
        // console.log(error.response);
        setLanguageSelectedVal(false)

      });
    };
    fetchData();
  }
  
  const _onTestPress = () => {
    // console.log(eP.apiUrl + eP.auth + '/' +phoneExtCode[0] + username +'/'+ password);
    const fetchData = async () => {
      await axios(
        eP.apiUrl + eP.auth + '/' + '90'+'-'+'demo' +'/'+ '_DE_mo'
      ).then(function (response) {
        // TODO bearer token buraya gelecektir
        // console.log('sakjhdaskhd')
        saveUserToken(response.data[0].id);
      })
      .catch(function (error) {

        
        if(!error.response)
          alert('System Error: Please Contact Administrator')
        else if (error.response.status == '401')
         alert('Your membership has not been activated yet.')
        else
        alert('Your Phone or Password is wrong')
        // console.log(error.response);
        setLanguageSelectedVal(false)

      });
    };
    fetchData();
  }



  const _onPressLogin = () => {

    if(!phoneExtCode)
      setLanguageSelectedVal(true)

    if (!username || !password || !phoneExtCode)  {
      alert( 'Please Fill in fields');
      return;
    } else {
      _onValidated();
    }
  };


    const saveUserToken = async (data) => {
      console.log('USER PW Invalidate edilecek');
      console.log('UserAttemptSayısı Artırılacak _UNSET_33_ gibi         // save userId + userToken to Redux');
      // try{
        AsyncStorage.setItem('userId',data)
        AsyncStorage.setItem('userToken', data)
        .then((data) => {
            console.log('dispatchec',data)
            // dispatch(loading(false));
            // dispatch(saveToken('token saved'));
            // console.log('askjdhajk')
            navigation.navigate('App');
        })
        .catch((err) => {
            dispatch(loading(false));
            dispatch(error(err.message || 'ERROR'));
        })
      // } catch (error) {
      //   console.log('sa' , error)
      // }
  
      }
      


  const _setLanguage= (lang) => {
      i18n.locale = lang;
      setLangSel(lang)
      let langText = lang == 'ru' ? 'Russian' : 'English'
      // alert('Your Application Will be in '+ langText )
  }

  const LanguageSelection = () => {
    return(
      <View>
        <Text style={styles.languageSelectionText}>{langSel == 'en' ? 'Select Language' : 'выберите язык' }  </Text>
        <View  style={styles.languageContainer}>
        <TouchableOpacity 
        style={styles.languageSelectionFlag}
        onPress={() => _setLanguage('en')}>
          <Image source={ require('../../assets/images/flag_en.png') } />
          <Text>English</Text>
        </TouchableOpacity>
        <TouchableOpacity
        style={styles.languageSelectionFlag}
        onPress={() => _setLanguage('ru')}>
          <Image source={ require('../../assets/images/flag_ru.png') } 
          />
          <Text>Россия</Text>

        </TouchableOpacity> 
        </View>
      </View>
    )
  }



  if(isLoggedIn)
    return (
      navigation.navigate('App')
    )
   else{
    return (
      <KeyboardAvoidingView style={styles.container}>
          <TouchableWithoutFeedback  style={styles.container} onPress={Keyboard.dismiss}>
          <ScrollView style={styles.sView}>

          <View  style={styles.container}>
            <Image style={styles.loginLogo} source={require('../../assets/images/logo.png')} />
            <View style={styles.form} >
              <Text></Text>
              <Text>{langSel == 'en' ? 'Sign In' : 'Войти в систему' } </Text>

                <View style={styles.formViewDouble}>
                <View 
                  style={[languageSelectedVal ? styles.widthThirtyFaulty : styles.widthThirty ]   }>
                <CountryPicker
                    {...{
                      countryCode,
                      withFilter,
                      withFlag,
                      withAlphaFilter,
                      withCallingCode,
                      onSelect,
                    }}
                   
                   />
                   </View>
                   <View 
                      style={styles.widthSeventy}>
                    <TextField  
                      label={langSel == 'en' ? 'Phone Number' : 'телефонный номер' } 
                      keyboardType='phone-pad'
                      onChangeText={un => setUsername(un)}/>
                  </View>
                  </View>
                <View >
                  <TextField  
                  label={langSel == 'en' ? 'Password' : 'пароль' } 
                  secureTextEntry={true}
                  autoCapitalize = 'none'
                  onChangeText={ text =>  setPassword(text) } />
                </View>
              <View style={{marginBottom:70}}>
              {!demoAccountShow ? <></> : (
              <TouchableOpacity
              style={{textAlign:'right', alignItems:'flex-end', marginBottom:40}}
                // onPress={() => navigation.navigate('ForgotPassword')}>
                onPress={() => _onTestPress()}>
                <Text style={{fontWeight:'100'}}>See Demo Account / Without Login</Text>
                </TouchableOpacity>
                )}
              <Button
                onPress={() => _onPressLogin()}
                title={langSel == 'en' ? 'Login' : 'Авторизоваться' } 
                color="#3897f1"
              />
              <TouchableOpacity
                style={{alignItems:'center',marginTop:40,borderBottomWidth:2, borderBottomColor:'#ccc', width:'60%', alignSelf:'center'}}
              onPress={() => navigation.navigate('UserCreate', {langSel: langSel})}>
                <Text>{langSel == 'en' ? 'Create a New Account' : 'Создать новый аккаунт' } </Text>
              </TouchableOpacity>
              </View>
              <LanguageSelection style={styles.languageContainer}/>
            </View>
          </View>
          </ScrollView>
          </TouchableWithoutFeedback> 
          </KeyboardAvoidingView>
        )
      }
  }



const styles = StyleSheet.create({
  sView: {
    height:"100%",
    width:"100%"
  },
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    height: "100%",
    width:"100%",
    flex:1,
    justifyContent: 'center',
  },
  inputView:{
    width:"80%",
    backgroundColor:"#465881",
    borderRadius:25,
    height:50,
    marginBottom:20,
    justifyContent:"center",
    padding:20
  },
  inputText:{
    height:50,
    color:"white"
  },

  loginLogo: {
    width: 200,
    height:200,
    resizeMode: 'contain',
    marginTop: 30
  },
  form:{
    flex:1,
    justifyContent : 'center',
    width: '80%'
  },
  inputView:{
    width:"80%",
    backgroundColor:"#465881",
    borderRadius:25,
    height:50,
    marginBottom:20,
    justifyContent:"center",
    padding:20
  },
  languageSelectionText: {
    fontWeight : '100',
    width: '100%',
    alignSelf:'center',
    justifyContent:'center',
    flex:1,
    textAlign: 'center',
    flexDirection: 'row',
  },
  languageContainer:{
    flex:1,
    marginTop:10,
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    alignItems:'center'
  },
  formViewDouble: {
    flex:1,
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop:0,
    paddingBottom:0
  },
  widthThirty: {
    width: '12%',
    zIndex:99,
    marginRight:10,
    marginTop:20,
    borderWidth:1, 
    borderColor:'#ccc',
    borderRadius:5
  },
  widthThirtyFaulty: {
    width: '12%',
    zIndex:99,
    marginRight:10,
    marginTop:20,
    borderWidth:1, 
    borderColor:'#ff0000',
    borderRadius:5
  },
  widthSeventy : {
    width: '85%',
  },
  languageSelectionFlag: {
    borderWidth:1,
    borderRadius:5,
    textAlign:'center',
    alignItems:'center',
    borderColor:'#ccc',
    backgroundColor:'#e2e2e2',
    padding:5,
    shadowColor:'#000',

  }

});


const mapStateToProps = state => ({
  token: state.token,
});


const mapDispatchToProps = dispatch => ({
  saveUserToken: () => dispatch(saveUserToken()),
});

connect(mapStateToProps, mapDispatchToProps)(LoginScreen);