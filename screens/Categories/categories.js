import React, { useState, useEffect, useFocusEffect } from "react";
import axios from "axios";
import {
  StyleSheet,
  Dimensions,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
  Image,
} from "react-native";
import { WebView } from "react-native-webview";

import { connect } from "react-redux";

import BottomNav from "../../components/BottomNav";

var eP = require("../../constants.js");

//TODO
// imageOnLoad
// iki kere geri basınca çıkmak istiyormusun diye sorsun


export default function MainCategories({ navigation }) {

  const [data, setData] = useState();
  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        eP.apiUrl + eP.categories
        ).then(function (response) {
          setData(response.data);
        }).catch(function (error) {
          if(!error.response)
            alert('System Error: Please Contact Administrator')
        });
    };
    fetchData();
  }, []);

  const _retrieveData = async (key) => {
    try {
      await AsyncStorage.getItem(key).then((res) => {
        console.log(res);
          axios.get(eP.apiUrl + 'checkUserValid/' + res)
          .then(function (resp){

            if(resp.data != true ){
            alert('Your membership is not valid')

              try {
                AsyncStorage.getAllKeys()
                    .then(keys => AsyncStorage.multiRemove(keys))
                    // .then(() => alert('success'));  
          } catch (error) {
            console.log(error);
          }
          
              try {
              AsyncStorage.removeItem('userToken')
                .then((data) => {
                    navigation.navigate('Auth')
                    dispatch(loading(false));
                    dispatch(removeToken(data));
                })
                .catch((err) => {
                    dispatch(loading(false));
                    dispatch(error(err.message || 'ERROR'));
                })
          
              } catch (error) {
                console.log(error);
              }
            }
          }).catch(function (error) {
            if(!error.response)
              alert('System Error: Please Contact Administrator')
          });
        //TODO : checkUserIdValid || status is not 1

      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    AsyncStorage.getItem("_CART_PRODUCT_").then((res) => {
      if (res == null) {
        try {
          AsyncStorage.setItem("_CART_PRODUCT_", JSON.stringify([]));
        } catch (e) {
          alert("Failed to save the data to the storage");
          console.log(e);
        }
      }
    });

    _retrieveData("userToken");
    _retrieveData("userId");




    const fetchData = async () => {
      const result = await axios(eP.apiUrl + eP.categories);
      setData(result.data);
    };
    fetchData();
  }, []);

  return (
    <View style={styles.container}>
      <FlatList
        style={styles.flatListTransulent}
        keyExtractor={(item, index) => index.toString()}
        data={data}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            onPress={() =>
              navigation.push("SubCategory", { catId: item.id, a: "AA" })
            }
          >
            <View style={styles.itemContent}>
              <Image
                style={
                  ([index == 0 ? styles.radiused : styles.noned],
                  styles.itemImage)
                }
                source={{ uri: item.image }}
              />
              {/* <View
                style={
                  ([index == 0 ? styles.radiused : styles.noned],
                  styles.itemOverlay)
                }
              /> */}
              {/* <Text style={styles.itemTitle}>{item.title}</Text> */}
            </View>
          </TouchableOpacity>
        )}
      />
      <BottomNav navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  item: {
    marginTop: 125,
    padding: 10,
    textAlign: "right",
    backgroundColor: "rgba(255,255,255,0.8)",
    fontSize: 24,
  },

  divisionFour: {
    width: Dimensions.get("window").width / 3 - 20,
    margin: 20,
    backgroundColor: "#000",
    marginBottom: 100,
  },
  flatListTransulent: {
    margin: 10,
    marginBottom: 65,
    height: "30%",
  },
  video: {
    height: 200,
    width: "100%",
  },
  itemContent: {
    padding: 20,
    margin: 5,
    position: "relative",
    marginHorizontal: Platform.OS === "ios" ? -15 : 0,
    height: 180,
  },
  itemTitle: {
    color: "#fff",
    fontSize: 40,
    marginTop: 60,
  },
  itemPrice: {
    color: "#fff",
    fontSize: 30,
  },
  itemImage: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  radiused: {
    borderRadius: 50,
    backgroundColor: "#000",
  },
  noned: {
    borderRadius: 0,
  },
  itemOverlay: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: "#6271da",
    opacity: 0.4,
  },
});

const mapStateToProps = (state) => ({
  token: state.token,
});

const mapDispatchToProps = (dispatch) => ({
  saveUserToken: () => dispatch(saveUserToken()),
});

connect(mapStateToProps, mapDispatchToProps)(MainCategories);
