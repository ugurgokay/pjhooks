import React, { useEffect, useState } from "react";
import axios from "axios";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground,
} from "react-native";
// import * as Analytics from 'expo-firebase-analytics'; 

import BottomNav from "../../components/BottomNav";

var eP = require("../../constants.js");

// TODO
// onRefresh reload

export default function SubCategory({ navigation }) {
  let mainCatId = navigation.getParam("catId");
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);


  // Analytics.logEvent('share', {
  //   contentType: 'text', 
  //   itemId: 'Expo rocks!', 
  //   method: 'facebook'
  // });


  useEffect(() => {
    // console.log(eP.apiUrl + eP.categories + "/" + mainCatId);
    const fetchData = async () => {
      const result = await axios(eP.apiUrl + eP.categories + "/" + mainCatId);
      setData(result.data);
      setLoading(false);
    };
    fetchData();
  }, []);

  const mainCatHandler = (id) => {
    navigation.navigate("List", { id: id });
  };

  return (
    <View style={[styles.container, styles.loading]}>
      {isLoading ? (
        <ActivityIndicator
          size="large"
          color="#0000ff"
          style={styles.loading}
        />
      ) : (
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          numColumns={2}
          style={styles.subCatList}
          data={data}
          keyExtractor={(item, index) => String(index)}
          renderItem={({ item }) => (
            <TouchableOpacity
              style={styles.alignFifth}
              onPress={() => mainCatHandler(item.id)}
            >
              <ImageBackground
                source={{ uri: item.image }}
                style={{
                  width: "100%",
                  height: 200,
                }}
                blurRadius={0.4}
              >
                <Text style={styles.item}> {item.title} </Text>
              </ImageBackground>
            </TouchableOpacity>
          )}
        />
      )}
      <BottomNav navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    paddingTop: 10,
  },
  item: {
    padding: 10,
    paddingLeft: 0,
    textAlign: "center",
    width: "100%",
    marginTop: 160,
    backgroundColor: "rgba(255,255,255,1)",
    color: "#000",
    fontSize: 16,
  },
  alignFifth: {
    width: "47%",
    margin: 5,
  },
  loading: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  subCatList: {
    marginBottom: 65,
  },
});
