import React, {useState} from 'react';
import {StyleSheet, Text, View, FlatList,Image, Linking} from 'react-native';
import { Icon } from "react-native-elements";

import BottomNav from '../components/BottomNav';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { abs } from 'react-native-reanimated';


export default function ContactPage({ navigation }) {
  const [cat, setCats] = useState([
    {
      name: "Mesih Paşa, Aksaray Cd. No: 3, 34130 \n Fatih/İstanbul",
      id: 1,
      icon: "location-arrow",
    },
    { name: "info@romaster.com.tr", id: 2, icon: "envelope" },
    { name: "+90(212) 528 04 44", id: 3, icon: "phone-square" },
    { name: "  +90(532) 245 62 44", id: 4, icon: "mobile-phone" },
    { name: "www.romaster.com.tr", id: 5, icon: "anchor" },
    { name: "/romasterofficial", id: 6, icon: "instagram" },
  ]);

  const openInMaps = () => {
    const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
    const latLng = `${'41.00930'},${'28.9553801'}`;
    const label = 'Romaster';
    const url = Platform.select({
      ios: `${scheme}${label}&ll=${latLng}`,
      android: `${scheme}${latLng}(${label})`
    });
    Linking.openURL(url)

  }

  return (
    <View style={styles.container}>
      <FlatList
        data={cat}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <View style={styles.alignItems}>
            <Icon
              style={styles.itemIcon}
              name={item.icon}
              size={20}
              color="#000"
              type="font-awesome"
            />
            <Text style={styles.item}> {item.name} </Text>
          </View>
        )}
      />
      <TouchableOpacity style={styles.officeMap} onPress={openInMaps}> 
   
      <Image style={{height:400,width:'100%'}} source={require('../assets/images/map.png')} />

      </TouchableOpacity>
      <BottomNav style={styles.fullWi} navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#fff',
        paddingTop:20
    },
    alignItems: {
        flex:1,
        flexWrap:'wrap',
        flexDirection:'row',
        margin:10
    },  
    item: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        fontSize:14
    },
    itemIcon: {
        paddingRight:10,
    },
    officeMap: {
        width:'100%',
        height:300
    }


})