import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Animated,
  ScrollView,
  Image,
  ActivityIndicator,
  TextInput,
  AsyncStorage,
  Dimensions,
  SafeAreaView,
} from "react-native";
import { Dropdown } from "react-native-material-dropdown";
import { TextField } from "react-native-material-textfield";
import { Icon } from "react-native-elements";
import axios from "axios";
import BottomNav from "../../components/BottomNav";


var eP = require("../../constants.js");

import FullScreenImage from "../../components/FullScreenImage";

// TODO
// fullscreenImage
// imageLoadingEffect
// Zoom Pinch efectini ekle ürüne
// onClick fullScreen efecti
// sepete ekleyince animasyon 

import en from "../../assets/language/en-EN";
import ru from "../../assets/language/ru-RU";

// import * as Localization from 'expo-localization';
import i18n from "i18n-js";

i18n.translations = {
  en,
  ru,
};

export default function ProductDetail({ navigation }) {
  const [value, onChangeText] = useState("5");
  const [totalValue, setTotalValue] = useState("0 $");

  const [productDetail, setProductDetail] = useState("");
  const [productDetailFeatures, setProductDetailFeatures] = useState("");
  const [productSubs, setProductSubs] = useState("");

  const [cartValidation, setCartValidation] = useState(false);
  let productId = navigation.getParam("item");
  let cartProduct = navigation.getParam("entireItem")



  // console.log(navigation.state)
  const [fullScreen, setFullscreen] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        eP.apiUrl + eP.productDetail + "/" + productId //'/2629fe43-414d-45ee-b96b-13b168786eec' //+ productId //1
      );
      setProductDetail(result.data[0]);
      setProductDetailFeatures(JSON.parse(result.data[0].productFeatures));
      setProductSubs(JSON.parse(result.data[0].productTypes));
    };
    fetchData();
    lastOnlineUpdate();


  }, []);
  



  const lastOnlineUpdate = async () => {
    try {
      await AsyncStorage.getItem("userId").then((res) => {
        axios
          .put(
            eP.apiUrl + eP.lastOnline + "/" + res,
            {
              lastOnlineRand: JSON.stringify("a"),
            }
            // { TODO buraya headers kesinlikle eklenecektir
            //    headers: {
            //        'api-token': 'xyz',
            //         //other header fields
            //    }
            // }
          )
          .then((res) => {
            // console.log(res, "done");
          })
          .catch((error) => {
            console.log(error);
          });
        return res;
      });
    } catch (error) {
      return null;
    }
  };

  const product = { productTypes: [] };

  const Item = ({ title }) => (
    <View>
      {title.value == "" ? (
        <></>
      ) : (
        <View style={styles.productFeatureItem}>
          <Text style={styles.ProductFeaturesTitle}>{title.item} </Text>
          <Text style={styles.ProductFeaturesTitle}>{title.value} </Text>
        </View>
      )}
    </View>
  );

  const ProductFeature = ({ node }) => (
    <>
    {node.quota < 0 ? ( <></>
        ) : (  
          <View style={styles.productUnit}>
      <View style={styles.unitBoxes}>
        <TextField
          label={i18n.t("lblSize")}
          value={node.productType}
          style={{ color: "#ff0000" }}
          baseColor="#0520b5"
          disabled
        />
      </View>
      <View style={styles.unitBoxesTwo}>
        <Dropdown
          label={i18n.t("lblSerieStock")}
          style={[cartValidation ? styles.unitItemsFaulty : styles.unitItems ]   }
          value={node.value}
          baseColor="#0520b5"
          data={node.quantity}
          onChangeText={(value) => {
            node.value = value;
            _calculateAmount();
          }}
        />
      </View>
      <View style={styles.unitBoxes}>
        <TextField
          label={i18n.t("lblUnitPrice")}
          baseColor="#0520b5"
          disabled
          style={styles.unitFontSize}
          value={node.productTotalPrice + " $"}
        />
      </View>
        </View>      )}
    </>
  );

  const _checkCartIsEmpty = async (pr) => {
    try {
      await AsyncStorage.getItem("_CART_PRODUCT_").then((item) => {
        let parsed = JSON.parse(item) || [];
        if (!(parsed.filter((e) => e.id === pr.id).length > 0)) {
          _saveData(pr);
        } else {
          alert(i18n.t("alrtAlreadyAdded"));
        }
      });
    } catch (error) {
      console.log("err", error);
    }
  };

  const _saveData = async (pr) => {
    try {
      const cartProducts = await AsyncStorage.getItem("_CART_PRODUCT_");
      const c = cartProducts ? JSON.parse(cartProducts) : [];
      c.push(pr);
      AsyncStorage.setItem("_CART_PRODUCT_", JSON.stringify(c));
      // navigation.push('Cart');
      navigation.pop();
    } catch (e) {
      alert("Failed to save the data to the storage");
      console.log(e);
    }
  };

  const _retrieveData = async (key) => {
    // let keys;
    try {
      await AsyncStorage.getItem(key).then((res) => {
        return res;
      });
    } catch (error) {
      return null;
    }
  };

  const _calculateAmount = () => {
    let firstSub =
      productSubs[0].productTotalPrice *
      productSubs[0].productSubPiece *
      productSubs[0].value;
    let price = parseInt(firstSub);
    if (productSubs.length == 2) {
      let secondSub =
        productSubs[1].productTotalPrice *
        productSubs[1].productSubPiece *
        productSubs[1].value;
      price = parseInt(firstSub + secondSub);
    }
    setTotalValue(price + " $");
  };

  const placeOrder = (pr) => {
    product.id = pr.id;
    product.productName = pr.productName;
    product.productName_ru = pr.productName_ru;
    product.picture = pr.picture;
    product.productTypes = productSubs;

    //TODO bug FIX ikinci ürün ile ilgili olan kısmı yani
    //TODO : alreadyy added kaldırılacak ve update edecek aynı ürünüde

    if (
      (!product.productTypes[0].value && product.productTypes.length == 1) ||
      (!product.productTypes[0].value && !product.productTypes[1].value)
    ){
      setCartValidation(true)
      alert(i18n.t("alrtSelectQuantity"));
    }
    else {
      _checkCartIsEmpty(product);
    }
  };

  let moveAnimation = new Animated.ValueXY({ x: 10, y: 450 });

  const _moveBall = () => {
    Animated.spring(moveAnimation, {
      toValue: { x: 100, y: -10 },
    }).start();
  };

  return (
    <View style={styles.container}>
      {productDetail == "" ? (
        <ActivityIndicator
          size="large"
          color="#0000ff"
          style={styles.loading}
        />
      ) : (
        <View style={styles.container} >
          <TouchableOpacity
          navigation={navigation}
          style={{flex:1,height:500}}
          onPress={()=> navigation.navigate("FullScreenImage", {picture: productDetail.picture})}>
          <Image
            style={styles.productImage}
            source={{ uri: productDetail.picture }}
          />
          </TouchableOpacity>
          <ScrollView style={styles.scrollable}>
            <Text style={styles.productTitle}>
              Code : {productDetail.productName}
            </Text>
            <SafeAreaView style={styles.container}>
              <FlatList
                data={productSubs}
                keyExtractor={(item, index) => String(index)}
                style={styles.productUnitContainer}
                renderItem={({ item }) => <ProductFeature node={item} />}
              />
            </SafeAreaView>
            <View style={styles.productItemContainer}>
              <Text style={styles.header}> {i18n.t("txtProductSpecs")} </Text>
              <FlatList
                data={productDetailFeatures.data}
                keyExtractor={(item, index) => String(index)}
                renderItem={({ item }) => <Item title={item} />}
              />
            </View>
          </ScrollView>

          {/* <Animated.View style={[styles.tennisBall, moveAnimation.getLayout()]}> */}
          <View style={styles.absoluteBottom}>
            <TouchableOpacity
              style={styles.addToCart}
              onPress={() => placeOrder(productDetail)}
            >
              <Icon
                name="cart-plus"
                size={20}
                color="#fff"
                type="font-awesome"
              />
              <Text style={styles.txtButtonAddToCart}>
                {i18n.t("btnAddToCart")}
              </Text>
              <TextInput
                style={styles.totalAmount}
                value={totalValue}
                disabled
              />
            </TouchableOpacity>
          </View>
          {/* </Animated.View> */}
        </View>
      )}
      <BottomNav navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  scrollable: {
    flex: 1,
  },
  unitItems: {
    borderWidth: 1,
    borderColor: "#0520b5",
    padding: 5,
    borderRadius: 5,
    color: "#ff0000",
    marginLeft: -5,
    paddingTop: Platform.OS === "ios" ? 5 : -5,
  },
  unitItemsFaulty: {
    borderWidth: 1,
    borderColor: "#ff0000",
    padding: 5,
    borderRadius: 5,
    color: "#ff0000",
    marginLeft: -5,
    paddingTop: Platform.OS === "ios" ? 5 : -5,
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  productItemContainer: {
    width: "100%",
    marginTop: 20,
    padding: 20,
    flex: 1,
    marginBottom: Platform.OS === "ios" ? 110 : 130,
  },
  productFeatureItem: {
    marginTop: 5,
    borderRadius: 5,
    padding: 5,
    justifyContent: "space-between",
    backgroundColor: "#eee",
    flexDirection: "row",
    flexWrap: "wrap",
    flex: 1,
  },
  unitFontSize: {
    fontSize: 20,
    color: "#ff0000",
  },
  ProductFeaturesTitle: {
    flexWrap: "wrap",
    justifyContent: "space-between",
    fontSize: 12,
    backgroundColor: "#eee",
    flexDirection: "row",
  },
  productImage: {
    height: "100%",
    width: "100%",
  },
  addToCart: {
    backgroundColor: "#0520b5",
    padding: 20,
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 60,
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    flexWrap: "wrap",
  },
  totalAmount: {
    backgroundColor: "#fff",
    padding: 10,
    borderRadius: 5,
  },
  absoluteBottom: {
    position: "absolute",
    width: "100%",
    bottom: 0,
    left: 0,
  },
  productUnit: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "space-evenly",
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 5,
  },
  productUnitContainer: {
    borderRadius: 5,
    borderColor: "#da5e16",
    borderWidth: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    flex: 1,
    margin: 10,
  },
  unitBoxes: {
    width: Dimensions.get("window").width / 4 - 20,
    flex: 2,
    margin: 5,
  },
  unitBoxesTwo: {
    width: Dimensions.get("window").width / 2 - 20,
    flex: 2,
    margin: 5,
  },
  productTitle: {
    fontWeight: "400",
    color: "#0520b5",
    fontSize: 24,
    padding: 10,
  },
  txtButtonAddToCart: {
    color: "#fff",
    fontSize: 20,
  },

  // tennisBall: {
  //   // display: 'flex',
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   backgroundColor: 'greenyellow',
  //   // borderRadius: 100,
  //   width: 100,
  //   height: 100,
  //   position:'absolute',
  //   bottom:199,
  //   zIndex: 999
  // },
  button: {
    paddingTop: 24,
    paddingBottom: 24,
  },
  buttonText: {
    fontSize: 24,
    color: "#333",
  },
});
