import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  RefreshControl,
  Text,
  Icon,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  AsyncStorage,
  Button,
  ActivityIndicator,
} from "react-native";

import BottomNav from "../../components/BottomNav";

// TODO
// refresh

import en from "../../assets/language/en-EN";
import ru from "../../assets/language/ru-RU";

import i18n from "i18n-js";

i18n.translations = {
  en,
  ru,
};

import axios from "axios";

var eP = require("../../constants.js");

export default function ProductList({ route, navigation }) {
  let subCatId = navigation.getParam("id");

  const [subProducts, setSubProducts] = useState([]);
  const [cart, setCart] = useState([]);

  const getData = () => {
    const fetchData = async () => {
      const result = await axios(eP.apiUrl + eP.productz + "/" + subCatId);
      setSubProducts(result.data);
    };
    fetchData();
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    try {
      AsyncStorage.getItem("_CART_PRODUCT_").then((value) => {
        setCart(JSON.parse(value));
      });
    } catch (error) {
      console.log(error);
    }
    subProducts.map((s) => {
      cart.map((c) => {
        if (s.id == c.id) s.addedToCart = "YES";
      });
    });
    // setSubProducts(subProducts)
  });

  const handleDetail = (item) => {
    navigation.navigate("Detail", { item: item.id });
  };
  const NoProductItem = ({}) => {
    return (
      <View
        style={
          (styles.productItem,
          {
            alignSelf: "center",
            alignItems: "center",
            justifyContent: "center",
            flex:1
          })
        }
      >
              <ActivityIndicator
          size="large"
          color="#0000ff"
          style={styles.loading}
        />
        <Text>{i18n.t("txtNoProductInView")}</Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {subProducts.length == 0 ? (
        <NoProductItem />
      ) : (
        <FlatList
          style={styles.flatListContainer}
          numColumns={2}
          data={subProducts}
          renderItem={({ item }) => (
            <TouchableOpacity
              style={[
                item.addedToCart == "YES"
                  ? styles.productItemCartAdded
                  : styles.productItem,
              ]}
              onPress={() => handleDetail(item)}
            >
              {item.addedToCart == "YES" ? (
                <Text style={styles.inCartText}>{i18n.t("txtInCart")} </Text>
              ) : (
                <View></View>
              )}
              <Image
                style={styles.productImage}
                source={{ uri: item.picture }}
              />
              <View style={styles.productInfoContainer}>
                <Text style={styles.productTitle}>{item.productName} </Text>
                <Text style={styles.productPrice}>{item.price} $ </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      )}

      <BottomNav navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
  },
  productItemCartAdded: {
    width: "47%",
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: "orange",
    margin: 5,
    backgroundColor: "#fff",
    paddingBottom: 10,
    borderRadius: 5,
    paddingRight: 10,
  },
  productItem: {
    width: "47%",
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: "rgb(5,25,138)",
    margin: 5,
    backgroundColor: "#fff",
    paddingBottom: 10,
    borderRadius: 5,
    paddingRight: 10,
  },
  inCartText: {
    textAlign: "right",
    color: "#fff",
    backgroundColor: "orange",
    position: "absolute",
    zIndex: 4,
    right: 10,
    top: 10,
    padding: 3,
  },
  productImage: {
    height: 175,
    // width:"100%",
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 3,
    justifyContent: "center",
    alignItems: "center",
  },
  productInfoContainer: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  productTitle: {
    justifyContent: "flex-start",
    width: "75%",
    color: "#0520b5",
    marginTop: 3,
  },
  productPrice: {
    color: "#ff0000",
    width: "25%",
    fontWeight: "600",
    letterSpacing: -1,
    fontSize: 18,
  },
  flatListContainer: {
    marginBottom: 65,
  },
});
