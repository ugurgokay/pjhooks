import React, { useEffect, useState, useLayoutEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  AsyncStorage,
  FlatList,
  Alert,
  Image,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Icon } from "react-native-elements";
import axios from "axios";

import en from "../../assets/language/en-EN";
import ru from "../../assets/language/ru-RU";

import i18n from "i18n-js";

i18n.translations = {
  en,
  ru,
};

var eP = require("../../constants.js");

import BottomNav from "../../components/BottomNav";

export default function Cart({ navigation }) {
  const [cartProducts, setCartProducts] = useState([]);
  const [totalCartPrice, setTotalCartPrice] = useState("");


  useEffect(() => {
    _retrieveData();
  }, []);

  useEffect(() => {
    let totalAmount = 0;
    cartProducts.forEach((b) => {
      b.productTypes.forEach((a) => {
        totalAmount += parseInt(
          a.productSubPiece * a.productTotalPrice * a.value
        );
      });
    });
    setTotalCartPrice(totalAmount);
  }, [cartProducts]);

  const _emptyCart = () => {
    setCartProducts([]);
    try {
      AsyncStorage.setItem("_CART_PRODUCT_", JSON.stringify([]));
    } catch (e) {
      alert("Failed to save the data to the storage");
      console.log(e);
    }
  };

  const _placeOrder = () => {
    try {
      AsyncStorage.getItem("userId").then((userId) => {
        axios
          .post(
            eP.apiUrl + eP.order,
            {
              orderDetail: JSON.stringify(cartProducts),
              orderType: 1,
              userId: userId,
            }
            // { TODO buraya headers kesinlikle eklenecektir
            //    headers: {
            //        'api-token': 'xyz',
            //         //other header fields
            //    }
            // }
          )
          .then((res) => {
            alert(i18n.t("alrtCartDone"));
            _emptyCart();
            navigation.navigate("OrderHistory", { lastOrder: res.data });
          })
          .catch((error) => {
            console.log(error);
          });
      });
    } catch (error) {
      console.log(error);
    }
  };

  const _retrieveData = async () => {
    try {
      AsyncStorage.getItem("_CART_PRODUCT_").then((value) => {
        setCartProducts(JSON.parse(value));
      });
    } catch (error) {
      console.log(error);
    }
  };

  const _removeItem = (item) => {
    const removedItems = cartProducts.filter((node) => node.id != item.id);
    setCartProducts(removedItems);
    return AsyncStorage.setItem("_CART_PRODUCT_", JSON.stringify(removedItems));
  };

  const _removeProductFromCart = (item) => {
    Alert.alert(
      i18n.t("txtRemove") + " " + i18n.t("txtItem"),
      i18n.t("qAreyouSureToremoveFromCart") +
        i18n.t("txtProduct") +
        " : " +
        item.productName,
      [
        {
          text: i18n.t("txtCancel"),
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: i18n.t("txtRemove"),
          onPress: () => {
            console.log("OK Pressed");
            _removeItem(item);
            // console.log(pr)
            // order(pr);
          },
        },
      ],
      { cancelable: false }
    );
  };

  const ProductItem = ({ item }) => (
    <View style={styles.productTypesView}>
      <Text style={{ width: "30%", color: "#ff0000" }}>
        {item.productType}{" "}
      </Text>
      <Text style={{ color: "#0520b5" }}>
        {item.value} {i18n.t("serie")}{" "}
      </Text>
      <Text style={{ color: "#ff0000" }}>{item.productTotalPrice} $ =</Text>
      <Text style={{ color: "#ff0000" }}>
        {" "}
        {item.value * item.productSubPiece * parseInt(item.productTotalPrice)} $
      </Text>
    </View>
  );

  const ReceivingProduct = ({ item }) => (
    <>
      <TouchableOpacity
        onPress={() => {
          _removeProductFromCart(item);
        }}
        style={styles.removeFromCatItemContainer}
      >
        <Text style={styles.removeFromCatItemContainerText}>
          {" "}
          {i18n.t("txtRemovefromCart")}{" "}
        </Text>
        <Icon
          name="remove"
          style={styles.removeFromCatItemContainerIcon}
          type="font-awesome"
          color="#ff0000"
          size={15}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate("Detail", { item: item.id })}
      >
        <View style={styles.cartItem}>
          <Image style={styles.cartItemImage} source={{ uri: item.picture }} />
          <View style={styles.eightyWidth}>
            <View style={styles.flexAlign}>
              <Text style={styles.titleProduct}>{item.productName} </Text>
            </View>
            <FlatList
              style={styles.cartSubProducts}
              data={item.productTypes}
              keyExtractor={(item, index) => String(index)}
              renderItem={({ item }) =>
                item.value ? <ProductItem item={item} /> : <></>
              }
            />
          </View>
        </View>
      </TouchableOpacity>
    </>
  );

  const NoCartItem = ({}) => {
    return (
      <View style={styles.noCartItem}>
        <Icon
          name="shopping-cart"
          type="font-awesome"
          style={{ marginBottom: 20 }}
        />
        <Text style={styles.noCartItemText}>{i18n.t("txtnoCartItem")}</Text>
        <TouchableOpacity
          style={{ marginTop: 270 }}
          onPress={() => navigation.navigate("MainCategory")}
        >
          <Icon name="home" type="font-awesome" />
          <Text style={styles.btnGotoMain}>{i18n.t("btngoToMainScreen")}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {cartProducts.length == 0 ? (
        <NoCartItem />
      ) : (
        <View style={styles.container}>
          <FlatList
            data={cartProducts}
            style={styles.mbFourty}
            keyExtractor={(item, index) => String(index)}
            renderItem={({ item }) => <ReceivingProduct item={item} />}
          />

          <View style={styles.absoluteBottom}>
            <View style={styles.divideTwoContainer}>
              <TouchableOpacity
                style={styles.widthFifty} 
              >
                <Text style={styles.white}> {i18n.t("txtTotal")} </Text>
                <Text style={styles.white}> {totalCartPrice} $</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.widthFifty}
                onPress={() => navigation.pop()}
              >
                <Text style={(styles.mixinTwenty, styles.white)}>
                  {" "}
                  {i18n.t("txtContinue")}{" "}
                </Text>
                <Icon
                  name="cart-arrow-down"
                  style={{ marginTop: 5, marginRight: 20 }}
                  size={25}
                  color="#fff"
                  type="font-awesome"
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={styles.bottomButton}
              onPress={() => _placeOrder()}
              // onPress={() => navigation.pop(2)}
            >
              <Icon
                name="thumbs-o-up"
                type="font-awesome"
                size={20}
                color="#fff"
                style={{ marginTop: 10 }}
              />
              <Text style={styles.white}> {i18n.t("txtOrderSend")} </Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
      <BottomNav navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  titleProduct: {
    color: "#0520b5",
  },
  container: {
    flex: 1,
    height: "100%",
    backgroundColor: "#e2e2e2",
  },
  absoluteBottom: {
    backgroundColor: "#ccc",
    position: "absolute",
    bottom: 0,
    marginBottom: 60,
    paddingBottom: 10,
    left: 0,
    paddingTop: 10,
    flex: 1,
    width: "100%",

  },
  bottomButton: {
    color: "#fff",
    backgroundColor: "#da5e16",
    justifyContent: "center",
    width: "90%",
    margin: 0,
    flexWrap: "wrap",
    flexDirection: "row",
    borderRadius: 10,
    marginLeft: 20,
    marginRight: 20,
  },
  white: {
    color: "#fff",
    margin: 10,
    fontSize: 16,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
  },
  cartItem: {
    backgroundColor: "#fff",
    padding: 10,
    margin: 10,
    marginTop: 0,
    borderBottomColor: "#e2e2e2",
    borderBottomWidth: 2,
    borderRightColor: "#e2e2e2",
    borderRightWidth: 2,
    borderRadius: 4,
    flexDirection: "row",
    flexWrap: "wrap",
    flex: 1,
  },
  cartItemImage: {
    width: "25%",
    height: 70,
    borderRadius: 5,
  },
  eightyWidth: {
    width: "70%",
    marginLeft: 10,
  },
  cartSubProducts: {
    marginTop: 2,
  },
  productTypesView: {
    flexDirection: "row",
    flexWrap: "wrap",
    backgroundColor: "#eee",
    margin: 2,
    padding: 2,
    borderRadius: 3,
    borderBottomColor: "#e6dbdb",
    borderBottomWidth: 1,
    borderRightColor: "#e6dbdb",
    borderRightWidth: 1,
    flex: 1,
    justifyContent: "space-between",
  },
  noCartItem: {
    backgroundColor: "#fff",
    flex: 1,
    borderRadius: 5,
    alignItems: "center",
    alignContent: "center",
    flexDirection: "column",
    flexWrap: "wrap",
    justifyContent: "center",
    margin: 20,
  },
  noCartItemText: {
    alignItems: "center",
    justifyContent: "center",
    fontWeight: "100",
  },
  btnGotoMain: {
    fontWeight: "700",
  },
  divideTwoContainer: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    width: "100%",
    paddingRight: 20,
    paddingLeft: 20,
    justifyContent: "space-between",
  },
  widthFifty: {
    width: "100%",
    backgroundColor: "#0520b5",
    color: "#fff",
    marginBottom: 10,
    flex: 1,
    marginTop: 5,
    flexWrap: "wrap",
    flexDirection: "row",
    padding: 0,
    borderRadius: 5,
  },
  mbFourty: {
    marginBottom: 170,
  },
  mixinTwenty: {
    marginTop: 20,
    marginLeft: 20,
  },
  flexAlign: {
    flexWrap: "wrap",
    flex: 1,
    flexDirection: "row",
  },
  removeFromCatItemContainer: {
    flexWrap: "wrap",
    flex: 1,
    flexDirection: "row",
    width: "45%",
    marginTop: 10,
    alignSelf: "flex-end",
    borderRadius: 5,
    marginBottom: 0,
    justifyContent: "flex-end",
    right: 15,
    marginBottom: 0,
    backgroundColor: "#ccc",
  },
  removeFromCatItemContainerText: {
    // fontWeight:'100',
    fontSize: 12,
    color: "#000",
    marginRight: 5,
    marginTop: 2,
  },
  removeFromCatItemContainerIcon: {
    borderRadius: 10,
    width: 20,
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 1,
  },
});
