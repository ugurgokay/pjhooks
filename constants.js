'use strict';

var CONSTANTS= function(){ }


// CONSTANTS.apiUrl = 'http://localhost:3001/';



CONSTANTS.apiUrl = 'http://185.224.139.140:3001/'
CONSTANTS.categories = 'categories';
CONSTANTS.products = 'products';
CONSTANTS.productz = '_prsds';
CONSTANTS.productDetail = 'productDetail';
CONSTANTS.lastOnline = 'lastOnline';
CONSTANTS.categoriesFeatures = 'categoriesFeatures';
CONSTANTS.user = 'user';
CONSTANTS.users = 'users';
CONSTANTS.order = 'order';
CONSTANTS.orders = 'orders';
CONSTANTS.news = 'news'

CONSTANTS.auth = 'auth';

module.exports = CONSTANTS;
